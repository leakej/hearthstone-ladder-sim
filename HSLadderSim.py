import trueskill
import random
import uuid
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
from numpy import mean, std, median, polyfit
from numpy.random import normal


class Rank:
    def __init__(self, level=25, stars=0, new_rules=False, donais_method=False):
        self.level = level
        self.stars = stars
        self.win_streak = 0
        self.new_rules = new_rules
        self.best_season = (25, 0)
        self.best_all = (25, 0)
        self.donais_method = donais_method
        if not new_rules:
            self.levels = {25:2, 24:2, 23:2, 22:2, 21:2,
                           20:3, 19:3, 18:3, 17:3, 16:3,
                           15:4, 14:4, 13:4, 12:4, 11:4,
                           10:5, 9:5, 8:5, 7:5, 6:5, 5:5, 4:5, 3:5, 2:5, 1:5}
        else:
            self.levels = {25:5, 24:5, 23:5, 22:5, 21:5,
                           20:5, 19:5, 18:5, 17:5, 16:5,
                           15:5, 14:5, 13:5, 12:5, 11:5,
                           10:5, 9:5, 8:5, 7:5, 6:5, 5:5, 4:5, 3:5, 2:5, 1:5}

    def win_game(self):
        if self.level > 0:
            self.win_streak += 1
            if self.win_streak > 2 and self.level > 5:
                self.stars += 2
            else:
                self.stars += 1
            if self.stars > self.levels[self.level]:
                self.adjust_level()
        if self.level < self.best_season[0]:
            self.best_season = (self.level, self.stars)
        elif self.level <= self.best_season[0] and self.stars > self.best_season[1]:
            self.best_season = (self.level, self.stars)
        if self.level < self.best_all[0]:
            self.best_all = (self.level, self.stars)
        elif self.level <= self.best_all[0] and self.stars > self.best_all[1]:
            self.best_all = (self.level, self.stars)

    def lose_game(self):
        if self.level > 0:
            self.win_streak = 0
            if self.level < 21:
                self.stars -= 1
                if self.stars < 0:
                    self.adjust_level()

    def adjust_level(self):
        if self.stars < 0:
            if self.level in [20, 15, 10, 5]:
                self.stars = 0
            else:
                self.level += 1
                self.stars = self.levels[self.level] - 1
        else:
            diff = self.stars - self.levels[self.level]
            self.level -= 1
            self.stars = diff

    def new_season(self):
        if not self.donais_method:
            self.level = self.best_season[0]
            self.stars = self.best_season[1]
        if self.new_rules:
            if self.level <= 21:
                self.level += 4
            else:
                self.level = 25
        else:
            bonus_stars = 25 - self.level
            self.level = 25
            self.stars = 0
            while bonus_stars > 0:
                if bonus_stars > self.levels[self.level]:
                    bonus_stars -= self.levels[self.level]
                    self.level -= 1
                else:
                    self.stars = bonus_stars
                    bonus_stars = 0

class Player:
    def __init__(self, mmr, rank, id=None):
        if id is None:
            self.id = uuid.uuid1()
        else:
            self.id = id
        self.mmr = trueskill.Rating(mu=mmr, sigma=4.0)
        self.rank = rank
        self.wins = 0
        self.losses = 0

class Ladder:
    def __init__(self, population_size, new_rules=False, import_pop=None):
        self.season_number = 1
        self.population_size = population_size
        self.new_rules = new_rules
        self.season_data = {}
        if import_pop:
            self.players = []
            for player in import_pop:
                self.players.append(
                    Player(mmr=trueskill.Rating(mu=player.mmr.mu, sigma=4.0),
                           rank=Rank(level=player.rank.level,
                                     stars=player.rank.stars,
                                     new_rules=new_rules),
                           id=player.id))
            if len(self.players) > population_size:
                self.players = random.sample(self.players, population_size)
        else:
            self.players = self.init_population()
        self.sort_players()

    def init_population(self):
        population = []
        ts = trueskill.global_env()
        SIGMA = ts.sigma / 2.0
        mmrs = normal(ts.mu, SIGMA, self.population_size)
        for mmr in mmrs:
            population.append(
                Player(mmr=mmr, rank=Rank(new_rules=self.new_rules)))
        return population

    def play_season(self, n, verbose=True, plot=False):
        self.season_data[self.season_number] = {
            'start':[(x.rank.level, x.mmr.mu) for x in self.players],
            'mid':[],
            'end':[]}
        game_count = 0
        interval = int(.1 * self.population_size)
        midseason = False
        for i in range(int(n/interval)):
            self.play_games(interval)
            game_count += interval
            if game_count >= n/2.0 and not midseason:
                if verbose:
                    print "===\nSeason #{0} [{1}|{2}] mid-season stats:".format(
                        self.season_number,
                        self.population_size, self.new_rules)
                    self.print_stats()
                self.season_data[self.season_number]['mid'] = [(x.rank.level, x.mmr.mu) for x in self.players]
                midseason = True
        self.season_data[self.season_number]['end'] = [(x.rank.level, x.mmr.mu) for x in self.players]
        if plot:
            self.plot_players(self.season_number)
            self.plot_mmr(self.season_number)
        self.new_season()

    def play_game(self):
        p1 = random.choice(self.players)
        p1_index = self.players.index(p1)
        p2 = self.players[
            random.choice(
                [x for x in [p1_index-1, p1_index+1] if x >= 0 and x < self.population_size])]
        p1_odds = win_probability(p1, p2)
        if random.random() <= p1_odds:
            p1.rank.win_game()
            p1.wins += 1
            p2.rank.lose_game()
            p2.losses += 1
        else:
            p2.rank.win_game()
            p2.wins += 1
            p1.rank.lose_game()
            p1.losses += 1

    def play_games(self, n):
        for i in range(n):
            self.play_game()
        self.sort_players()

    def sort_players(self):
        self.players = sorted(
            self.players,
            key=lambda z: z.rank.level + (1.0 / (2 + z.rank.stars)))

    def new_season(self):
        print "===\nSeason #{0} [{1}|{2}] final stats:".format(
            self.season_number, self.population_size, self.new_rules)
        self.season_number += 1
        self.print_stats()
        for player in self.players:
            player.rank.new_season()
        print "===\nSeason #{0} initial stats:".format(self.season_number)
        self.print_stats()

    def print_stats(self):
        print '---'
        rankings = {}
        for i in self.players:
            try:
                rankings[i.rank.level].append(i.mmr.mu)
            except KeyError:
                rankings[i.rank.level] = [i.mmr.mu]
        for i in sorted(rankings.keys(), reverse=True):
            print "Rank {0}: {1} players. Average mmr: {2}".format(
                i, len(rankings[i]), mean(rankings[i]))
        print ''

    def reset_players(self, new_rules):
        for player in self.players:
            player.rank = Rank(new_rules=new_rules)
            player.wins = 0
            player.losses = 0

    def plot_players(self, season):
        plt.clf()
        index = np.arange(26)
        fig, ax = plt.subplots()
        start_bins = []
        mid_bins = []
        end_bins = []
        for i in range(26):
            start_bins.append(
                [x[0] for x in self.season_data[season]['start']].count(
                    i)/float(self.population_size))
            mid_bins.append(
                [x[0] for x in self.season_data[season]['mid']].count(
                    i)/float(self.population_size))
            end_bins.append(
                [x[0] for x in self.season_data[season]['end']].count(
                    i)/float(self.population_size))
        bar_width = .3
        if season > 1:
            start_bg = ax.bar(index - bar_width,
                              start_bins,
                              color='green',
                              label="Start",
                              width=bar_width,
                              align='center')
        mid_bg = ax.bar(index,
                        mid_bins,
                        color='orange',
                        label="Midseason",
                        width=bar_width,
                        align='center')
        end_bg = ax.bar(index + bar_width, end_bins,
                        color='red',
                        label="End",
                        width=bar_width,
                        align='center')
        ax.set_xticklabels(('L', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                            '10', '11', '12', '13', '14', '15', '16', '17',
                            '18', '19', '20', '21', '22', '23', '24', '25'),
                           fontsize=8)
        ax.autoscale(tight=True)
        if self.new_rules:
            ruleset = "New Rules"
        else:
            ruleset = "Current Rules"
        ax.set_xlabel('Rank')
        ax.set_ylabel('% Players')
        ax.set_title('Players Per Rank, {0}, Season {1}'.format(
            ruleset, season), fontsize=9)
        ax.set_xticks(index + bar_width / 2)
        ax.legend()
        plt.xlim((0, 25))
        plt.ylim((0, .50))
        plt.savefig('players_{0}_{1}.png'.format(ruleset, season))

    def plot_mmr(self, season):
        plt.clf()
        if self.new_rules:
            ruleset = "New Rules"
        else:
            ruleset = "Current Rules"
        fig, ax = plt.subplots()
        if season > 1:
            x_series = [x[0] for x in self.season_data[season]['start']]
            y_series = [x[1] for x in self.season_data[season]['start']]
            scatter = ax.scatter(
                x_series, y_series, alpha=0.1, color='green', label="Start")
        x_series = [x[0] for x in self.season_data[season]['mid']]
        y_series = [x[1] for x in self.season_data[season]['mid']]
        scatter = ax.scatter(
            x_series, y_series, alpha=0.1, color='orange', label="Midseason")
        x_series = [x[0] for x in self.season_data[season]['end']]
        y_series = [x[1] for x in self.season_data[season]['end']]
        scatter = ax.scatter(
            x_series, y_series, alpha=0.1, color='red', label="End")
        ax.set_xlabel('Rank')
        ax.set_ylabel('Trueskill')
        ax.set_title('Trueskill Distribution By Rank, {0}, Season {1}'.format(
            ruleset, season), fontsize=9)
        ax.set_xticks(np.arange(26))
        ax.set_xticklabels(('L', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                            '10', '11', '12', '13', '14', '15', '16', '17',
                            '18', '19', '20', '21', '22', '23', '24', '25'),
                           fontsize=8)
        leg = ax.legend()
        for l in leg.legendHandles:
            l.set_alpha(1)
        ax.grid(True)
        plt.xlim((0, 25))
        plt.savefig('trueskill_{0}_{1}.png'.format(ruleset, season))

def win_probability(p1, p2):
    delta_mu = p1.mmr.mu - p2.mmr.mu
    sum_sigma = (p1.mmr.sigma ** 2) + (p2.mmr.sigma ** 2)
    size = 2
    ts = trueskill.global_env()
    denom = np.sqrt(size * (ts.beta * ts.beta) + sum_sigma)
    return ts.cdf(delta_mu / denom)


if __name__ == '__main__':
    current_ladder = Ladder(population_size=1000, new_rules=False)
    new_ladder = Ladder(population_size=1000,
                        new_rules=True,
                        import_pop=current_ladder.players)
    for season in range(12):
        current_ladder.play_season(100000, plot=True, verbose=True)
    for season in range(12):
        new_ladder.play_season(100000, plot=True, verbose=True)
